import  os, sys, time
from datetime import datetime, timedelta ,timezone 

files = os.listdir('.')

CET = timezone(timedelta(hours=+1.0))
UTC = timezone(timedelta(hours=+0.0))

eventList = []

for file in files:
    if '.ics' in file:
        with open(sys.path[0]+os.sep+file,"r") as icsfile:
            eventIndex = 0

            print("List of All Events:")
            for line in icsfile:
                if line[:8] == 'SUMMARY:':
                    eventName = line[8:]
                    print(eventName, end=': ')

                elif line[:13] == 'DTSTART;TZID=': #print all timestamps behind the eventName
                    print(line[13:])
                    if line[:24] == 'DTSTART;TZID=Europe/CET:':
                        timestamp = line[24:].strip()+"+0100"
                        eventTime = datetime.strptime(timestamp,"%Y%m%dT%H%M%S%z") 

                    elif line[:32] == 'DTSTART;TZID=Atlantic/Reykjavik:':
                        timestamp = line[32:].strip()+"+0000"
                        eventTime = datetime.strptime(timestamp,"%Y%m%dT%H%M%S%z") 

                elif line[:9] == 'SEQUENCE:':
                    eventOrder = line[9:]

                elif line[:10] == 'END:VEVENT':
                    if datetime.now(CET) + timedelta(hours=-6) > eventTime:
                        pass
                    elif datetime.now(CET) + timedelta(days=14) < eventTime:
                        pass
                    else:
                        eventIndex += 1
                        eventList.append([eventName,eventTime])
            print ("Upcoming events: ",eventIndex+1)
            print(eventList)

eventList = sorted(eventList,key= lambda x: x[1])

original1 = """<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
    <title>Trackmania World Tour Calendar Project</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <div class="navbar">
      <a href="webcal://tmwt.echtnurich.de/tmwt.ics">Subscribe to Calendar</a>
      <a href="https://tmwt.echtnurich.de/tmwt.ics">Download Calendar</a>
      <a href="https://gitlab.com/echtnurich/tmwt-cal">GitLab Project</a>
    </div>

    <h1>Trackmania World Tour Calendar</h1>

    <p>
      This has been forked from iCal Generator Project to distribute the TMWT calendar. </br>
      Challenger League matches are being held 1h later than planned by the rulebook. Take the times with a grain of salt.
    </p>
    
    <h2>Upcoming events:</h2>
"""
original2 = """
  </body>
</html>"""
with open(sys.path[0]+os.sep+"index.html","w+") as htmlfile:
    htmlfile.write(original1)
    htmlfile.write("<table style=\"text-align: left\">")
    resultDict = eventList
    for element in resultDict:
        htmlfile.write("<tr>")
        htmlfile.write("<th>"+element[0]+"</th><td>"+"   <b>UTC:</b> "+element[1].astimezone(UTC).strftime("%Y-%m-%d %H:%M:%S")+" <b>Discord:</b> &lt;t:"+str(int(time.mktime(element[1].astimezone(UTC).timetuple())))+":f&gt; </td>")
        htmlfile.write("</tr>")
    if len(resultDict) == 0:
        htmlfile.write("<tr><th>No race sessions scheduled within the next two weeks.</th></tr>")
    htmlfile.write("</table>")
    htmlfile.write("<p></p>")
    htmlfile.write("<p>Check UTC Time <a href=\"https://time.is/UTC\"><b>here</b></a>!</p>")
    htmlfile.write("<p>Copy Discord Timestamps into any Discord chat. Check out <a href=\"https://r.3v.fi/discord-timestamps/\">this</a> to learn more.</p>")
    htmlfile.write(original2)