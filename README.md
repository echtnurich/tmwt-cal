# iCal project

This is basically the plain HTML pages Template with some python stuck to it.

You can find the json/ics code in the ``generator``-Folder.  
ezjson.py is a rudimentary assistant for creating the json file.  
main.py generates ics files from it.

This whole repo is configured to automatically compile all json files in the repo root into ics files and push them into public/ics.  
From there, public/ics/html.py generates a basic file overview index.html, and all that gets published to [ical.echtnurich.de](https://ical.echtnurich.de).

## Are some entries wrong or out of date?
Contact me via the means described on [My Homepage](https://echtnurich.de)  
Or even open a merge request to let me merge and publish your changes directly.

Thanks